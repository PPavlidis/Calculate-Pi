# Calculate Pi
This project contains the tasks for Parallel and Distributed Computing course. 

These following programs calculate pi's value. <br />
Exerc3_1 just use threads to achieve its goal.<br />
Exerc3_2 contains a Master server who sends to its Workers the specified part of pi that it has to calculate.<br />
Exerc3_3 mixes both Exerc3_1 and Exerc3_2. Client makes threads to do his task.
